import os
import zipfile

import PyInstaller.__main__ as py

from mumstatic import NAME, CWD, VERSION


def retrieve_file_paths(dirName):
    filePaths = []
    for root, directories, files in os.walk(dirName):
        for filename in files:
            filePath = os.path.join(root, filename)
            filePaths.append(filePath)
    return filePaths


def release():
    os.chdir('dist/%s' % NAME)
    filePaths = retrieve_file_paths('.')
    zip_file = zipfile.ZipFile('../../release/%s_%s.zip' % (NAME, VERSION), 'w')
    with zip_file:
        for file in filePaths:
            zip_file.write(file)


if __name__ == "__main__":
    py.run([
        '--name=%s' % NAME,
        os.path.join(CWD, 'mumstatic', '__main__.py')
    ])
    release()
