import os
import os.path

import requests

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import TimeoutException

from mumstatic import CWD


class InvalidLogin(Exception):
    pass


def runtastic(username, password, driver):
    filename = None
    chrome_options = Options()
    # chrome_options.add_argument("--headless")

    chrome_driver = CWD + "/" + driver

    driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver)

    driver.get("https://www.runtastic.com/")

    driver.find_element_by_css_selector(".js-login-bttn").click()
    driver.find_element_by_xpath("//input[@type='email']").send_keys(username)
    driver.find_element_by_xpath("//input[@type='password']").send_keys(password)
    driver.find_element_by_css_selector(".js-submit").click()

    wait = WebDriverWait(driver, 10)
    # try:
    #     wait.until(ec.visibility_of_element_located((By.LINK_TEXT, "Export")))
    # except TimeoutException:
    #     driver.close()
    #     raise InvalidLogin
    # driver.find_element_by_link_text("Export").click()
    try:
        wait.until(ec.visibility_of_element_located((By.LINK_TEXT, "Exporter")))
    except TimeoutException:
        driver.close()
        raise InvalidLogin
    driver.find_element_by_link_text("Exporter").click()

    wait.until(ec.visibility_of_element_located((By.LINK_TEXT, "Télécharger")))

    try:
        driver.find_elements_by_link_text("Exporter")[1].click()
    except IndexError:
        driver.find_element_by_link_text("Télécharger").get_property("href")
        link = driver.find_element_by_link_text("Télécharger").get_property("href")
    # wait.until(ec.visibility_of_element_located((By.LINK_TEXT, "Download")))

    # try:
    #     driver.find_elements_by_link_text("Export")[1].click()
    # except IndexError:
    #     driver.find_element_by_link_text("Download").get_property("href")
    #     link = driver.find_element_by_link_text("Download").get_property("href")
        filename = link.split("/")[-1]
        if not os.path.isfile(filename):
            r = requests.get(link, allow_redirects=True)
            with open(filename, "wb") as f:
                f.write(r.content)
    driver.close()
    return filename


if __name__ == "__main__":
    runtastic()
