import json
import os
import shutil

from datetime import datetime, timedelta
from copy import deepcopy
from zipfile import ZipFile

PATH = "./temp/Sport-sessions"
DATA = {"distance": [], "duration": [], "elevation_gain": [], "elevation_loss": []}


def prepare_file(filename):
    shutil.rmtree("temp", ignore_errors=True)
    print('Decompress %s' % filename)
    with ZipFile(filename, "r") as zipObj:
        zipObj.extractall("temp")
    shutil.rmtree("temp/Sport-sessions/GPS-data", ignore_errors=True)
    shutil.rmtree("temp/Sport-sessions/Elevation-data", ignore_errors=True)


def fetch_file_data():
    return [name for root, dirs, files in os.walk(PATH) for name in files]


def load_data(filename):
    with open("%s/%s" % (PATH, filename)) as f:
        return json.loads(f.read())


def format_data(data):
    try:
        return {
            "start_time": datetime.fromtimestamp(data["start_time"] / 1000),
            "distance": data["distance"] / 1000,
            "duration": data["duration"],
            "elevation_gain": data.get("elevation_gain", 0),
            "elevation_loss": data.get("elevation_loss", 0),
        }
    except OSError:
        print(data)
        raise


def save_data(data):
    with open("export.json", "w") as f:
        f.write(json.dumps(data))


def calcstat(data):
    return {
        "distance": sum(data["distance"]),
        "duration": str(timedelta(milliseconds=sum(data["duration"]))),
        "elevation_gain": sum(data["elevation_gain"]),
        "elevation_loss": sum(data["elevation_loss"]),
        "activity": len(data["distance"]),
    }


class StatRuntastic:
    def __init__(self, filename):
        self.data = {}
        prepare_file(filename)
        self.listfile = fetch_file_data()
        self.file = filename.split("-")[1]

    def add(self, data):
        dyear = data["start_time"].year
        dmonth = data["start_time"].month
        if dyear not in self.data:
            self.data[dyear] = dict(
                {i: deepcopy(DATA) for i in range(1, 13)}, **deepcopy(DATA)
            )
        for i in ["distance", "duration", "elevation_gain", "elevation_loss"]:
            self.data[dyear][i].append(data[i])
            self.data[dyear][dmonth][i].append(data[i])

    @property
    def statsbyyear(self):
        return {
            "date": self.file,
            **{
                y: dict(month={i: calcstat(v[i]) for i in range(1, 13)}, **calcstat(v))
                for y, v in self.data.items()
            },
        }


def parse_data(filename):
    s = StatRuntastic(filename)
    for i in s.listfile:
        data = format_data(load_data(i))
        s.add(data)
    save_data(s.statsbyyear)


if __name__ == "__main__":
    s = StatRuntastic("export-20190824-000.zip")
    for i in s.listfile:
        data = format_data(load_data(i))
        s.add(data)
    save_data(s.statsbyyear)
