import json
import tkinter

import matplotlib.pyplot as plt

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

from mumstatic.fetch_data import runtastic, InvalidLogin
from mumstatic.parse_data import parse_data
from mumstatic.update import update_drv
from mumstatic import CWD, VERSION


def loaddata(filename):
    with open(CWD + '/' + filename, "r") as f:
        return json.loads(f.read())


def retrieve_data_year(data, type):
    sortkeys = sorted([int(x) for x in data.keys() if x.isdigit()])
    return {str(k): data[str(k)][type] for k in sortkeys}


def autolabel(rects, ax):
    for rect in rects:
        height = rect.get_height()
        ax.annotate(
            "%d" % height,
            xy=(rect.get_x() + rect.get_width() / 2, height),
            xytext=(0, -20),  # 3 points vertical offset
            textcoords="offset points",
            ha="center",
            va="bottom",
            color="white",
        )


class UpdatePopup(tkinter.Toplevel):
    filename = None

    def __init__(self, root):
        tkinter.Toplevel.__init__(self, root)
        self.initialize()

    def initialize(self):
        self.title('Mise a jour')
        self.protocol('WM_DELETE_WINDOW', self.close)
        self.resizable(0, 0)
        self.grid()
        self.form()

    def close(self):
        self.destroy()

    def form(self):
        tkinter.Button(self, text="MAJ Mumstatic", command=self.updatepg).grid(row=0, padx=5, pady=5)
        tkinter.Button(self, text="MAJ Chromedriver", command=self.updatedrv).grid(row=1, padx=5, pady=5)

    def updatepg(self):
        pass

    def updatedrv(self):
        self.filename = update_drv()
        self.close()


class ConfigPopup(tkinter.Toplevel):
    def __init__(self, root, login, passwd):
        tkinter.Toplevel.__init__(self, root)
        self.root = root
        self.login = login
        self.passwd = passwd
        self.initialize()

    def initialize(self):
        self.title('Configuration')
        self.protocol('WM_DELETE_WINDOW', self.close)
        self.resizable(0, 0)
        self.grid()
        self.form()

    def close(self):
        self.login = self.elogin.get()
        self.passwd = self.epasswd.get()
        self.destroy()

    def cancel(self):
        self.login = ''
        self.passwd = ''
        self.destroy()

    def form(self):
        tkinter.Label(self, text="Configuration Identifiant Runtastic").grid(columnspan=2, row=0)
        tkinter.Label(self, text="Login : ").grid(row=1, padx=5, pady=5)
        text = tkinter.StringVar(self, value=self.login)
        self.elogin = tkinter.Entry(self, textvariable=text, width=40)
        self.elogin.grid(row=1, column=1, padx=5, pady=5)
        tkinter.Label(self, text="Mot de passe : ").grid(row=2, padx=5, pady=5)
        text = tkinter.StringVar(self, value=self.passwd)
        self.epasswd = tkinter.Entry(self, textvariable=text, width=40)
        self.epasswd.grid(row=2, column=1, padx=5, pady=5)
        tkinter.Button(self, text="Ok", command=self.close).grid(row=3, column=0, padx=5, pady=5)
        tkinter.Button(self, text="Cancel", command=self.cancel).grid(row=3, column=1, padx=5, pady=5)


class MumstaticIHM(tkinter.Tk):
    configfile = 'config.json'
    graph = True
    _message = ''

    def __init__(self, root):
        tkinter.Tk.__init__(self, root)
        self.title("Mumstatic (v%s)" % VERSION)
        w, h = self.winfo_screenwidth(), self.winfo_screenheight()
        self.geometry("%dx%d+0+0" % (w, h))
        self.protocol('WM_DELETE_WINDOW', self.close)
        self.root = root
        try:
            self.data = loaddata("export.json")
        except FileNotFoundError:
            self.graph = False
        self.initialize()

    def close(self):
        self.quit()
        self.destroy()

    def initialize(self):
        self.menu()
        self.grid()
        self.bottom()
        if self.graph:
            self.draw()
            self.message = "Dernier MAJ : %s" % self.data["date"]
        self.loadcfg()

    def menu(self):
        menubar = tkinter.Menu(self)
        menu = tkinter.Menu(menubar, tearoff=0)
        menu.add_command(label="Configuration", command=self.parametre)
        menu.add_command(label="Raffraichir donnee", command=self.refresh)
        menu.add_separator()
        menu.add_command(label="Mis a jour", command=self.maj)
        menu.add_separator()
        menu.add_command(label="Quitter", command=self.close)
        menubar.add_cascade(label="Menu", menu=menu)
        self.config(menu=menubar)

    def draw(self):
        distance_year = retrieve_data_year(self.data, "distance")
        distance_month = retrieve_data_year(
            self.data[sorted(self.data.keys())[-2]]["month"], "distance"
        )
        fig, ((axs1), (axs2)) = plt.subplots(nrows=2, ncols=1)
        r1 = axs1.bar(list(distance_year.keys()), list(distance_year.values()))
        axs1.set_title("Par annee")
        r2 = axs2.bar(list(distance_month.keys()), list(distance_month.values()))
        axs2.set_title("Par mois")
        autolabel(r1, axs1)
        autolabel(r2, axs2)
        canvas = FigureCanvasTkAgg(fig, master=self)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)

    def bottom(self):
        self.bottomlabel = tkinter.Label(self, text=self.message)
        self.bottomlabel.pack(side='bottom')

    @property
    def message(self):
        return self._message

    @message.setter
    def message(self, message):
        self._message = message
        self.bottomlabel.config(text=message)
        self.update()

    def loadcfg(self):
        try:
            data = loaddata(self.configfile)
            self.login = data.get('username', '')
            self.passwd = data.get('password', '')
            self.driver = data.get('driver', '')
        except FileNotFoundError:
            self.login = ''
            self.passwd = ''
            self.driver = ''

    def savecfg(self):
        with open(self.configfile, 'w') as f:
            f.write(json.dumps({
                'username': self.login,
                'password': self.passwd,
                'driver': self.driver
            }))

    def parametre(self):
        self.w = ConfigPopup(self, self.login, self.passwd)
        self.wait_window(self.w)
        if self.w.login != '' and self.w.passwd != '':
            self.login = self.w.login
            self.passwd = self.w.passwd
            self.savecfg()

    def maj(self):
        self.w = UpdatePopup(self)
        self.wait_window(self.w)
        if self.w.filename:
            self.driver = self.w.filename
            self.savecfg()

    def refresh(self):
        self.message = 'Chargement'
        try:
            filename = runtastic(self.login, self.passwd, self.driver)
            if filename:
                parse_data(filename)
                self.graph = True
                self.data = loaddata("export.json")
                self.initialize()
        except InvalidLogin:
            self.message = 'Identifiant invalide'


if __name__ == "__main__":
    app = MumstaticIHM(None)
    app.mainloop()
