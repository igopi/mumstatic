import os

from zipfile import ZipFile

import requests

from mumstatic import CWD

DRVWIN = 'https://chromedriver.storage.googleapis.com/76.0.3809.126/chromedriver_win32.zip'
DRVUX = 'https://chromedriver.storage.googleapis.com/76.0.3809.126/chromedriver_linux64.zip'


def update_pgm():
    pass


def update_drv():
    os.chdir(CWD)
    drv = DRVWIN if os.name == 'nt' else DRVUX
    r = requests.get(drv, allow_redirects=True)
    with open('chromedriver.zip', "wb") as f:
        f.write(r.content)
    with ZipFile('chromedriver.zip', "r") as zipObj:
        zipObj.extractall("temp/drv")
    os.remove('chromedriver.zip')
    for element in os.listdir('temp/drv'):
        if 'chromedriver' in element:
            os.rename('temp/drv/' + element, element)
            return element
    return None
